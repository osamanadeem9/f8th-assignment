# F8th Task:

## 1. Task details

Your task is to create a native Android app. The app must be hidden into the background in the initial loading state. When the button **volume down** is pressed 3 times consecutively, it will toggle the visibility of the app (hidden/visible). 

## 2. Overview of the app

The app uses Accessibility Service to detect KeyEvents (volumeDown press). Since it's a service, it can work even when the app is not in foreground. When the **volumeDown button is pressed 3 times consecutively**, the app visibility is toggled (visible/hidden). [Demo Video](https://drive.google.com/file/d/1kEgOhu8ApJJqyiAju9c15_mqxU47seQ_/view?usp=drivesdk)

## 3. Assumptions
The app assumes the following:

 - Supported on Android phones > 5.0
 - The phone must not be locked while the app is being used. Else it won't capture the KeyEvents (in most of the updated Android versions)
 - The app has not been in background for a very long time. Otherwise the service may die (which can later be handled using a persistent foreground notification though)


## 4. How to run

 - The code can be simply built and run on Android Studio > 4.0.
 - Otherwise you can easily install the app by downloading the apk file [from here](https://bitbucket.org/osamanadeem9/f8th-assignment/src/master/demo-app.apk). 

## 5. Code explained

### i) MainActivity.java:

This is the launcher activity of our app. It has a simple UI, containing a centered button for **gaining accessibility permissions.** When the activity is launched, it also shows a dialog box with a brief explanation of why the app requires accessibility services.


### ii) KeyPressAccessibilityService.java:

This file contains the basic functionality of our app. It does the following things:

 - Captures KeyPress Events through the accessibility service.
 - This function will detect volumeDown button press for our app and toggle app visibility.
- The app counts the number of times volumeDown button is pressed.
- If the volumeDown button is pressed 3 times, the app visibility will be toggled (visible/hidden).
- The appVisibility status is received from AppClass.java and stored in Shared Prefs.

>> **Note:** This services extends AccessibilityService class and overrides onKeyEvent function to deliver the main functionality of our app.


### iii) AppClass.java:

It implements **Application.ActivityLifecycleCallbacks** to get the lifecycle callbacks of our MainActivity, and checks if the app is currently in background, or foreground. 

### iv) accessibility_service_xml_config.xml:
Sets the XML attributes for the configuration of our accessibility service. Following are the two important properties for detecting our KeyEvents.
>> android:canRequestFilterKeyEvents="true"  
android:accessibilityFlags="flagRequestFilterKeyEvents"

### v) AndroidManifest.xml:

We have to add the following permission to our manifest, for using accessibility services.
>> android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE"

And also the following configuration file is referenced in the KeyPressAccessibilityService metadata.
>>   android:name="android.accessibilityservice"
	android:resource="@xml/accessibility_service_xml_config" />`

## 6. Thanks

Lastly, thank you to the F8th team for giving me this opportunity. I hope that my solution is able to meet your standards, and I am giving the chance to compete further. Thank you very much, once again!

