package com.liteappz.f8thapplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SharedPrefUtils {
    public static void saveValueInt(Context mContext, String key, int value){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void saveValueBoolean(Context mContext, String key, boolean value){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static int retrieveValueInt(Context mContext, String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        int name = preferences.getInt(key, 0);
        return name;
    }

    public static boolean retrieveValueBoolean(Context mContext, String key){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        boolean name = preferences.getBoolean(key, false);
        return name;
    }

}
