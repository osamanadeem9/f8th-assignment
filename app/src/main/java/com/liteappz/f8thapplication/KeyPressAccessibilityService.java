package com.liteappz.f8thapplication;

import android.accessibilityservice.AccessibilityService;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.widget.Toast;

public class KeyPressAccessibilityService extends AccessibilityService {

    String TAG = this.getClass().getSimpleName();
    final int COUNTER_VAL = 3;    // default counter value, on which app visibility state will toggle
    int current_counter_val = 0;    // current value of counter, the value of which is changed or reset based on volumeDown clicks.
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        Log.d(TAG, "onAccessibilityEvent: ");

    }

    /*
    * Captures KeyPress Events through the accessibility service
    * This function will detect volumeDown button press for our app and toggle app visibility
    * The app counts the number of times volumeDown button is pressed
    * If the volumeDown button is pressed 3 times, the app visibility will be toggled (visible/hidden)
    * The appVisibility status is received from AppClass.java and stored in Shared Prefs.
    * */
    @Override
    public boolean onKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();

        // gets app visibility status from Shared Prefs (saved by AppClass.class using activity lifecycles)
        boolean isAppVisible = SharedPrefUtils.retrieveValueBoolean(this, "appVisibility");
        if (action == KeyEvent.ACTION_UP) {

            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
                Log.d(TAG, "Counter: " + current_counter_val);
                Toast.makeText(this, String.format("VolumeDown: %d", current_counter_val+1), Toast.LENGTH_SHORT).show();
                toggleAppVisibility(isAppVisible);
                return true;
            }
        }
        return false;
    }

    public void toggleAppVisibility(boolean isAppVisible){
        current_counter_val++;
        /*
         * if user presses volumeDown button for three times consecutively, the if condition is invoked.
         * if app is in the foreground, it toggles its state and sends the app to background.
         * if app is in the background, it toggles its state and sends the app to foreground.
         */
        if (current_counter_val==COUNTER_VAL){
            if (isAppVisible){
                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory(Intent.CATEGORY_HOME);
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeIntent);
            }
            else {
                Intent intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }
            // resets the counter once the app visibility is toggled.
            current_counter_val = 0;
        }

        /*
        * as soon as user presses the volumeDown button (counter is set to 1), a handler is invoked to run after 1000ms.
        * This is done to ensure user presses the volumeDown button consecutively, else the counter value is reset to zero.
        * */
        if (current_counter_val==1){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    current_counter_val = 0;
                }
            }, 1000);
        }
    }

    @Override
    public void onInterrupt() {
        Log.d(TAG, "onInterrupt: ");

    }
}