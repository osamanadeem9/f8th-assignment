package com.liteappz.f8thapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button allowAccessibilityButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        allowAccessibilityButton = findViewById(R.id.allowPermissionButton);

        // check if accessibility service is enabled, else displays the alert dialog
        if (!isAccessServiceEnabled(this, KeyPressAccessibilityService.class))
            accessibilityAlertDialog(this);

        // launches the accessibility settings screen on button click
        allowAccessibilityButton.setOnClickListener(v->{
            Intent accessibilityIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
            startActivity(accessibilityIntent);
        });

    }

    public void accessibilityAlertDialog(Context context){
        new AlertDialog.Builder(context)
                .setTitle(R.string.accessibility_permissions)
                .setMessage(R.string.accessibility_dialog_exp)

                // allows user to enable accessibility for our app on button click
                .setPositiveButton(R.string.allow_permission, (dialog, which) -> {
                    dialog.dismiss();

                    Intent accessibilityIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                    startActivity(accessibilityIntent);

                })
                .setNegativeButton(android.R.string.no, (dialog, which) ->{
                    finish();
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public boolean isAccessServiceEnabled(Context context, Class accessibilityServiceClass)
    {
        // checks if accessibility service is enabled, and returns a boolean value
        String prefString = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        return prefString!= null && prefString.contains(context.getPackageName() + "/" + accessibilityServiceClass.getName());
    }
}