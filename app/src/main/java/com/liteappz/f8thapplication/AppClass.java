package com.liteappz.f8thapplication;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AppClass extends Application implements Application.ActivityLifecycleCallbacks {

    String TAG = this.getClass().getSimpleName();

    private boolean isMainActivityVisible;

    /*
    * creates this Application class and registers ActivityLifecycleCallbacks.
    * gets the activity lifecycles of our MainActivity
    * In case of onStart or onResume callback, it sets application visibility status to visible and stores in Shared Prefs (for toggling app state in KeyPressAccessibilityService)
    * In case of onPause, onResume or onDestroy callback, it sets application visibility status to false and stores in Shared Prefs (for toggling app state)
    */
    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        setApplicationVisibleStatus(activity);
        Log.d(TAG, "onActivityStarted");
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        setApplicationVisibleStatus(activity);
        Log.d(TAG, "onActivityResumed");
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        setApplicationHiddenStatus(activity);
        Log.d(TAG, "onActivityPaused");
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        setApplicationHiddenStatus(activity);
        Log.d(TAG, "onActivityStopped");
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        setApplicationHiddenStatus(activity);
        Log.d(TAG, "onActivityDestroyed");
    }

    // gets MainActivity visibility status and saves to Shared Prefs (for use by our KeyPressAccessibilityService).
    private void setApplicationVisibleStatus (Activity activity) {
        if (activity instanceof MainActivity) {
            isMainActivityVisible = true;
            SharedPrefUtils.saveValueBoolean(this, "appVisibility", isMainActivityVisible);
        }
    }

    // gets MainActivity visibility status and saves to Shared Prefs (for use by our KeyPressAccessibilityService).
    private void setApplicationHiddenStatus (Activity activity) {
        if (activity instanceof MainActivity) {
            isMainActivityVisible = false;
            SharedPrefUtils.saveValueBoolean(this, "appVisibility", isMainActivityVisible);
        }
    }
}
